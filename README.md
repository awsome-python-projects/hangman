# Hangman Game

This repository contains a simple implementation of the classic hangman game. The game allows users to input a series of characters, that will checked to find out the `chosen_word`. Players have 6 attempts to guess the word, receiving hints as letters are revealed one by one with each correct guess.

## How to Play

- Guess a letter in the word.
- If the guessed letter is correct, you earn 5 points.
- If the guessed letter is incorrect, you lose a life by 1 and you lose points by 2.
- Win the game by guessing all letters correctly before running out of lives.

## Components

- `stages`: A list containing the hangman images displayed as the user loses lives.
- `hangman_words`: Where users input their list of words.
- `chosen_word`: The randomly selected word from the `hangman_words`.
- `display`: Tracks the correctly guessed letters.
- `lives`: The number of lives the user has remaining.
- `points`: The number of points the user has earned.
- `end`: Determines the game's progress and outcome.

## Gameplay Loop

The game is played in a loop until the `end` variable is set to `True`. This occurs when the user either wins by guessing all the letters or loses by running out of lives. During each turn:
- The user is prompted to guess a letter.
- The code checks if the guessed letter is in the `chosen_word`.
- Correct guesses update the `display`, while incorrect guesses decrement `lives`.
- The current hangman image and remaining lives are displayed.
- The game ends when the user either wins or loses, and the chosen word is revealed.

Enjoy the game and good luck!