stages = ['''
  +---+
  |   |
  O   |
 /|\  |
 / \  |
      |
=========
''', '''
  +---+
  |   |
  O   |
 /|\  |
 /    |
      |
=========
''', '''
  +---+
  |   |
  O   |
 /|\  |
      |
      |
=========
''', '''
  +---+
  |   |
  O   |
 /|   |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
  |   |
      |
      |
=========
''', '''
  +---+
  |   |
  O   |
      |
      |
      |
=========
''', '''
  +---+
  |   |
      |
      |
      |
      |
=========
''']
from hangman_words import hangman_words, hangman_ascii
# a=1
# word_list = []
display = []
words = ''
end = False
lives = 6
points = 0

print(hangman_ascii)
print('\n Welcome to Hangman Game: Epic Word Guessing Adventure! 🎩✨')

print('''
1. You have 6 attempts to unravel the mystery word... Choose wisely! No deductions for correct guesses, lucky you!
2. Each correct guess earns you 5 points - cha-ching! 💰
3. For each wrong guess, 2 points vanish into the abyss of lost points... poof! 🌪️

Get ready to dive into the world of words and win those points like a boss! 🏆💬
''')


# This code is valuable when, player wants to add there own sets of words to guess.
# b = int(input("How many words you want in your dictionary to guess later? "))

# while a<=b:
#     addition = input(f'What is word {a}, you want to be in your dictionary? ')
#     word_list.append(addition)
#     a += 1

chosen_word=random.choice(hangman_words)

for j in chosen_word:
    words += j
    words = words.lower()

for _ in words:
    display += '_'

print(display)

while end == False:
    guess = input('What is your guess? ').lower()
    if guess in display:
        print(f"You've already guessed '{guess}' letter.")
    for i in range(len(words)):
        letter = words[i]
        if guess == letter:
            display[i] = letter
            points += 5
            print(f'Points collected so far: {points}')

    if guess not in words:
        print(f"You've guessed a '{guess}' letter, that's not in the word. You lose a life and points too.")
        lives -= 1
        points -= 2
        if points < 0:
            points = 0
        if lives == 0:
            end = True
            print('You lose...')
    print(display, '\n', stages[lives])
    print('\n', f'Lives you got left... {lives}', '\n', f'Points you got so far... {points}')

    if '_' not in display:
        end = True
        print('\nYou WIN!')

print(f'Your total point pool is: {points}')
print(f"The word was: {chosen_word}")